CREATE SEQUENCE sweet_url_sequence
  start 10000
  increment 10;

CREATE TABLE sweet_url(
  id bigint NOT NULL DEFAULT nextval('sweet_url_sequence') PRIMARY KEY,
  user_id UUID REFERENCES users (id) ON DELETE CASCADE,
  original_url VARCHAR(2000) NOT NULL,
  short_url_fragment VARCHAR(128),
  created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  deleted TIMESTAMP WITH TIME ZONE
);

-- Sequence will be dropped when table is dropped
ALTER SEQUENCE sweet_url_sequence OWNED BY sweet_url.id;
