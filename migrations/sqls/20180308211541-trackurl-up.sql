CREATE TABLE track_url(
  id UUID PRIMARY KEY,
  sweet_url_id bigint REFERENCES sweet_url (id) ON DELETE CASCADE,
  ip_address VARCHAR(45),
  device_type VARCHAR(500),
  browser VARCHAR(500),
  os VARCHAR(500),
  referer VARCHAR(1000),
  created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
