CREATE OR REPLACE VIEW vw_users_sweet_url AS
  SELECT concat(u.id,s.id) as users_sweet_url_id
    , u.id as user_id
    , u.username
    , u.email
    , s.id as sweet_url_id
    , s.original_url
    , s.short_url_fragment
    , s.created
  FROM users u
  INNER JOIN sweet_url s ON u.id = s.user_id;
