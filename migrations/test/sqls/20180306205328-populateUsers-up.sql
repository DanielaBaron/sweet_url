-- someawesomepassword
INSERT INTO users(id, username, email, password) VALUES (
  (select gen_random_uuid()),
  'manuelspencer',
  'mspencer@random.com',
  '$2a$04$z04s1Faq2Rmx7mbBgxHE.O.87szjQP2BfJd33romGC5m6T9/BzdPy'
);
