const useragent = require('useragent')
const db = require('../db')

module.exports = {
  track: async (req, urlID) => {
    const uaString = req.headers['user-agent']
    const agent = useragent.parse(uaString)
    const ip = req.headers['x-forwarded-for'] ||
         req.connection.remoteAddress ||
         req.socket.remoteAddress ||
         req.connection.socket.remoteAddress
    const ref = req.get('Referrer')

    const q = 'INSERT INTO track_url(id, sweet_url_id, ip_address, device_type, browser, os, referer) VALUES ' +
      '((select gen_random_uuid()), $1, $2, $3, $4, $5, $6)'
    const values = [urlID, ip, agent.device.family, agent.family, agent.os.family, ref]
    try {
      await db.executeQuery({text: q, values: values})
    } catch (err) {
      throw (err)
    }
  }
}
