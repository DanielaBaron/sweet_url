const alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
const base = alphabet.length

// https://coligo.io/create-url-shortener-with-node-express-mongo/
module.exports = {
  // utility function to convert base 10 integer to base 58 string
  encode: (num) => {
    let encoded = ''
    while (num) {
      const remainder = num % base
      num = Math.floor(num / base)
      encoded = alphabet[remainder].toString() + encoded
    }
    return encoded
  },

  // utility function to convert a base 58 string to base 10 integer
  decode: (str) => {
    let decoded = 0
    while (str) {
      const index = alphabet.indexOf(str[0])
      const power = str.length - 1
      decoded += index * (Math.pow(base, power))
      str = str.substring(1)
    }
    return decoded
  }
}
