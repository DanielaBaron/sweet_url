const base58 = require('../util/encode-decode')
const db = require('../db')
const shortUrlUtil = require('../util/short-url-util')

module.exports = {
  getHandler: async (req, res) => {
    const originalDbSeqId = base58.decode(req.params.shorturl)
    const q = 'SELECT original_url FROM sweet_url WHERE id = $1'
    try {
      const dbRes = await db.executeQuery({text: q, values: [originalDbSeqId]})
      if (dbRes.rows && dbRes.rows.length === 1) {
        shortUrlUtil.track(req, originalDbSeqId)
        res.redirect(dbRes.rows[0].original_url)
      } else {
        res.status(404).send({code: '404.1', message: 'short url not found'})
      }
    } catch (err) {
      res.status(500).send({code: '500.1', message: 'Database error'})
    }
    // if originalDbSeqId is not an int, 400 bad request
  }
}
