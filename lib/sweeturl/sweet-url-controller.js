const db = require('../db')
const base58 = require('../util/encode-decode')

module.exports = {
  getAllHandler: async (req, res) => {
    // a real app would have auth middleware to know who is the authenticated user
    // add offset/limit to implement default pagination (eg: 0/10)
    const q = 'SELECT sweet_url_id, original_url, short_url_fragment, created FROM vw_users_sweet_url ' +
      'WHERE user_id = (SELECT id FROM users where email = \'mspencer@random.com\') ' +
      'ORDER BY created DESC'
    try {
      const dbRes = await db.executeQuery(q)
      // if time, extract dupe logic re: shortUrl building
      const result = dbRes.rows.map(r => {
        return {
          id: r.sweet_url_id,
          originalUrl: r.original_url,
          shortUrl: `${req.protocol}://${req.hostname}/${r.short_url_fragment}`,
          created: r.created
        }
      })
      res.json(result)
    } catch (err) {
      res.status(500).send({code: '500.1', message: 'Database error'})
    }
  },

  createHandler: async (req, res) => {
    // validate req.body.originalUrl is a valid url
    // if originalUrl has already been shortened, return it right away

    // a real app would have auth middleware to know who is the authenticated user
    const q = 'INSERT INTO sweet_url(user_id, original_url) ' +
      'VALUES((SELECT id from users where email=\'mspencer@random.com\'), $1) RETURNING *'
    const updateQ = 'UPDATE sweet_url SET short_url_fragment = $1 where id = $2'
    try {
      const dbRes = await db.executeQuery({text: q, values: [req.body.originalUrl]})
      // we need to get id from row just created for encoding
      const rowId = dbRes.rows[0].id
      const shortUrlFragment = base58.encode(rowId)
      // all queries should be part of transaction
      await db.executeQuery({text: updateQ, values: [shortUrlFragment, rowId]})
      // for dev mode also need to include port (unless Chrome host includes it?)
      const shortUrl = `${req.protocol}://${req.hostname}/${shortUrlFragment}`
      res.status(201).send({shortUrl: shortUrl})
    } catch (err) {
      res.status(500).send({code: '500.1', message: 'Database error'})
    }
  },

  getStatsHandler: async (req, res) => {
    const q = 'SELECT ip_address, device_type, browser, os, created FROM track_url WHERE sweet_url_id = $1'
    try {
      const dbRes = await db.executeQuery({text: q, values: [req.params.sweeturlid]})
      const result = dbRes.rows.map(r => {
        return {
          ipAddress: r.ip_address,
          deviceType: r.device_type,
          browser: r.browser,
          os: r.os,
          created: r.created
        }
      })
      res.json(result)
    } catch (err) {
      res.status(500).send('Database error')
    }
  }
}
