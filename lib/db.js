const { Pool } = require('pg')

const dbHost = process.env.DB_HOST || 'localhost'
const dbUser = process.env.DB_USER || 'shortsweet'
const dbName = process.env.DB_NAME || 'shortsweet'
const dbPass = process.env.DB_PASSWORD || 'shorturlsaresweet'
const dbPort = process.env.DB_PORT || 5432
const maxPool = process.env.DB_POOL_SIZE || 10

const pool = new Pool({
  host: dbHost,
  user: dbUser,
  database: dbName,
  password: dbPass,
  port: dbPort,
  max: maxPool
})
console.log(`Connected to Postgres at ${dbHost}:${dbPort}`)

module.exports = {
  executeQuery: async function (q) {
    const client = await pool.connect()
    try {
      const res = await client.query(q)
      return res
    } catch (err) {
      // logger...
      console.log(err.stack)
      throw (err)
    } finally {
      client.release()
    }
  }
}
