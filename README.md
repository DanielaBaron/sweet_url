# Short and Sweet

> A sweet url shortener

## Setup

### Postgres

``` shell
docker pull postgres
docker volume create shortsweet_data
docker run --name shortsweet_db \
	-v short_sweet_data:/var/lib/postgresql/data \
	-e POSTGRES_USER=shortsweet \
	-e POSTGRES_PASSWORD=shorturlsaresweet \
	-e POSTGRES_DB=shortsweet \
	-e PGDATA=/var/lib/postgresql/data \
	-p 5432:5432 \
  -d postgres
npm install
./node_modules/db-migrate/bin/db-migrate up
./node_modules/db-migrate/bin/db-migrate up:test
```

```
npm install -g nodemon
```

## Development

### Migrations

To add a new one:

```shell
./node_modules/db-migrate/bin/db-migrate create migrationName
```

## Reference

[Axios](https://github.com/axios/axios)
[Bulma CSS](https://bulma.io/documentation/overview/start/)
[DB Migration Docs](https://db-migrate.readthedocs.io/en/latest/)
[Node Postgres](https://node-postgres.com/)
[Vue Router](https://router.vuejs.org/en/)
[Vuex Example](https://github.com/vuejs/vuex/blob/dev/examples/shopping-cart/components/ShoppingCart.vue)
