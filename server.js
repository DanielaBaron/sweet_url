// include express module
const express = require('express')
// handle POST
const bodyParser = require('body-parser')

// instantiate express server
const app = express()
// handle JSON bodies
app.use(bodyParser.json())
// handles URL encoded bodies
app.use(bodyParser.urlencoded({ extended: true }))
// logging
const winston = require('winston')
const expressWinston = require('express-winston')

// init db
require('./lib/db')
// route handlers
const sweetUrlController = require('./lib/sweeturl/sweet-url-controller')
const shortUrlController = require('./lib/sweeturl/short-url-controller')

const fs = require('fs')
const path = require('path')

const indexHTML = (() => {
  // return path relative to server.js file
  return fs.readFileSync(path.resolve(__dirname, './index.html'), 'utf-8')
})()

// customize http request/response log messages
app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console({
      colorize: true
    })
  ],
  meta: false,
  msg: 'HTTP {{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms'
}))

// serve static assets in dist dir
app.use('/dist', express.static(path.resolve(__dirname, './dist')))

// should only be used for development: require dev server module and pass in app reference
// this extends server with two new modules
require('./build/dev-server')(app)

// routes
app.post('/api/sweeturl', sweetUrlController.createHandler)
app.get('/api/sweeturl', sweetUrlController.getAllHandler)
app.get('/api/sweeturl/stats/:sweeturlid', sweetUrlController.getStatsHandler)
app.get('/:shorturl', shortUrlController.getHandler)

// all other GET requests return index.html
app.get('*', (req, res) => {
  res.write(indexHTML)
  res.end()
})

app.use(expressWinston.errorLogger({
  transports: [
    new winston.transports.Console({
      colorize: true
    })
  ]
}))

// specify a port
const port = process.env.PORT || 3000
// listen on port and display callback
app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`)
})

// global error handling
app.use(function (err, req, res, next) {
  // logger.error('Unhandled exception.', {reason: err, stack: err.stack, nextCb: next})
  console.log(err)
  res.status(500).end()
})
process.on('uncaughtException', function (err) {
  console.log(err)
  // logger.error('Unhandled exception.', {reason: err, stack: err.stack})
  // cleanup()
})

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
})
