import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './theme/Home.vue'
import Details from './theme/Details.vue'
import NotFound from './theme/NotFound.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  linkActiveClass: 'is-active',
  scrollBehavior: (to, from, savedPosition) => ({ y: 0 }),
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {path: '/details/:id', name: 'details', component: Details}
      ]
    },
    { path: '/', redirect: '/home' },
    { path: '*', component: NotFound }
  ]
})

export default router
