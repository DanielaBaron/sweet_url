import axios from 'axios'

axios.defaults.baseURL = '/api'

// add response interceptor for generic error handling?

const apiService = {
  async getSweetUrls (page) {
    try {
      const res = await axios.get('/sweeturl')
      return res
    } catch (err) {
      console.error(err)
    }
  },

  async addSweetUrl (longUrl) {
    try {
      const res = await axios.post('/sweeturl', {originalUrl: longUrl})
      return res
    } catch (err) {
      console.error(err)
    }
  },

  async getSweetUrlDetails (id) {
    try {
      const res = await axios.get(`/sweeturl/stats/${id}`)
      return res
    } catch (err) {
      console.error(err)
    }
  }
}

export default apiService
