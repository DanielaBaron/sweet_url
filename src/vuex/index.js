import Vue from 'vue'
import Vuex from 'vuex'
import apiService from '../api'
import router from '../router'

// include Vuex in Vue
Vue.use(Vuex)

// initialize app state
const state = {
  sweetUrls: [],
  userAction: null,
  sweetUrlDetails: {}
}

// store accepts a json object which is the state
const store = new Vuex.Store({
  state,
  getters: {
    sweetUrls: state => state.sweetUrls,
    userAction: state => state.userAction,
    sweetUrlDetails: state => state.sweetUrlDetails
  },
  actions: {
    async loadSweetUrls (context) {
      const apiResponse = await apiService.getSweetUrls()
      context.commit('updateSweetUrls', apiResponse.data)
    },
    createRequested (context) {
      context.commit('updateUserAction', 'create')
      // navigate? home?userAction=create
    },
    async addSweetUrl (context, longUrl) {
      await apiService.addSweetUrl(longUrl)
      context.commit('updateUserAction', null)
      await this.dispatch('loadSweetUrls')
      // navigate? home
    },
    showDetails (context, sweetUrlID) {
      router.push({name: 'details', params: {id: sweetUrlID}})
    },
    async loadSweetUrlDetails (context, sweetUrlID) {
      const apiResponse = await apiService.getSweetUrlDetails(sweetUrlID)
      context.commit('updateSweetUrlDetails', apiResponse.data)
    }
  },
  mutations: {
    updateSweetUrls (state, data) {
      state.sweetUrls = data
    },
    updateUserAction (state, data) {
      state.userAction = data
    },
    updateSweetUrlDetails (state, data) {
      state.sweetUrlDetails = {rawClicks: data}
    }
  }
})

export default store
